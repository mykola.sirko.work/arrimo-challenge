import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Connection } from 'typeorm';
import { SubscriptionModule } from './subscription/subscription.module';
import { NewsletterModule } from './newsletter/newsletter.module';
import { AuthorModule } from './author/author.module';

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'postgres',
      host: 'localhost',
      port: 5432,
      username: 'postgres',
      password: 'root',
      database: 'challenge',
      entities: ['dist/**/*.entity{.ts,.js}'],
      synchronize: true,
      autoLoadEntities: true,
    }),
    SubscriptionModule,
    NewsletterModule,
    AuthorModule,
  ],
})
export class AppModule {
  constructor(private connection: Connection) {}
}
