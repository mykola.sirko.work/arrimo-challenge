export interface AuthorDeleteResponse {
  isAuthorDeleted: boolean;
}
