import {
  Column,
  CreateDateColumn,
  Entity,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { Newsletter } from '../../newsletter/entities/newsletter.entity';

@Entity()
export class Author {
  @PrimaryGeneratedColumn({})
  id: number;

  @Column()
  fullName: string;

  @OneToMany(() => Newsletter, (newsletter) => newsletter.author, {
    nullable: true,
  })
  newsletters: Newsletter[];

  @CreateDateColumn()
  createdAt: Date;
}
