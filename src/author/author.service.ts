import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { Author } from './entities/author.entity';
import { AuthorParameter } from './parameters/author.parameter';
import { AuthorDeleteResponse } from './responses/author-delete.response';

@Injectable()
export class AuthorService {
  constructor(
    @InjectRepository(Author)
    private authorRepository: Repository<Author>,
  ) {}

  public async create(data: AuthorParameter): Promise<Author> {
    const author = new Author();

    author.fullName = data.name;
    author.newsletters = null;

    try {
      return this.authorRepository.save(author);
    } catch (e) {
      throw new HttpException(
        'Author create error',
        HttpStatus.INTERNAL_SERVER_ERROR,
      );
    }
  }

  public async findAll(filter: any = {}): Promise<Author[]> {
    try {
      return this.authorRepository.find(filter);
    } catch (e) {
      throw new HttpException(
        'Authors find error',
        HttpStatus.INTERNAL_SERVER_ERROR,
      );
    }
  }

  public async findOne(filter: any): Promise<Author> {
    try {
      return this.authorRepository.findOne(filter);
    } catch (e) {
      throw new HttpException(
        'Author find unexpected error',
        HttpStatus.INTERNAL_SERVER_ERROR,
      );
    }
  }

  public async update(id: number, data: AuthorParameter): Promise<Author> {
    try {
      return this.authorRepository.save({ id, ...data });
    } catch (e) {
      throw new HttpException(
        'Author update error',
        HttpStatus.INTERNAL_SERVER_ERROR,
      );
    }
  }

  public async remove(id: number): Promise<AuthorDeleteResponse> {
    try {
      return {
        isAuthorDeleted:
          (await this.authorRepository.delete({ id })).affected === 1,
      };
    } catch (e) {
      throw new HttpException(
        'Author remove error',
        HttpStatus.INTERNAL_SERVER_ERROR,
      );
    }
  }
}
