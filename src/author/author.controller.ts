import {
  Controller,
  Param,
  Body,
  Query,
  Get,
  Post,
  Patch,
  Delete,
} from '@nestjs/common';
import { AuthorService } from './author.service';
import { AuthorParameter } from './parameters/author.parameter';
import { Author } from './entities/author.entity';
import { AuthorDeleteResponse } from './responses/author-delete.response';
import { ApiTags } from '@nestjs/swagger';

@ApiTags('Author')
@Controller('api/author')
export class AuthorController {
  constructor(private readonly authorService: AuthorService) {}

  @Post()
  public async create(@Body() data: AuthorParameter): Promise<Author> {
    return this.authorService.create(data);
  }

  @Get()
  public async findAll(
    @Query() filter: AuthorParameter = {},
  ): Promise<Author[]> {
    return this.authorService.findAll({
      where: filter,
      relations: ['newsletters.subscriptions'],
    });
  }

  @Get(':id')
  public async findOne(@Param('id') id: string): Promise<Author> {
    return this.authorService.findOne({
      where: { id: +id },
      relations: ['newsletters.subscriptions'],
    });
  }

  @Patch(':id')
  public async update(
    @Param('id') id: string,
    @Body() data: AuthorParameter,
  ): Promise<Author> {
    return this.authorService.update(+id, data);
  }

  @Delete(':id')
  public async remove(@Param('id') id: string): Promise<AuthorDeleteResponse> {
    return this.authorService.remove(+id);
  }
}
