import { ApiProperty } from '@nestjs/swagger';

export class AuthorParameter {
  @ApiProperty({ type: 'string', example: 'J. K. Rowling' })
  public readonly name?: string;
}
