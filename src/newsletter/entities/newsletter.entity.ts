import {
  Column,
  CreateDateColumn,
  Entity,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { Author } from '../../author/entities/author.entity';
import { Subscription } from '../../subscription/entities/subscription.entity';

@Entity()
export class Newsletter {
  @PrimaryGeneratedColumn({})
  id: number;

  @Column()
  name: string;

  @Column()
  description: string;

  @ManyToOne(() => Author, (author) => author.newsletters, {
    nullable: false,
  })
  author: Author;

  @OneToMany(() => Subscription, (subscription) => subscription.newsletter, {
    nullable: false,
  })
  subscriptions: Subscription[];

  @CreateDateColumn()
  createdAt: Date;
}
