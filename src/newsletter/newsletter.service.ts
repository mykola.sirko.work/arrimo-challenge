import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { NewsletterDeleteResponse } from './responses/newsletter-delete.response';
import { NewsletterParameter } from './parameters/newsletter.parameter';
import { Newsletter } from './entities/newsletter.entity';

@Injectable()
export class NewsletterService {
  constructor(
    @InjectRepository(Newsletter)
    private newsletterRepository: Repository<Newsletter>,
  ) {}

  public async create(data: NewsletterParameter): Promise<Newsletter> {
    const newsletter = new Newsletter();

    newsletter.name = data.name;
    newsletter.description = data.description;
    newsletter.author = data.author;
    newsletter.subscriptions = [];

    try {
      return this.newsletterRepository.save(newsletter);
    } catch (e) {
      throw new HttpException(
        'Newsletter create error',
        HttpStatus.INTERNAL_SERVER_ERROR,
      );
    }
  }

  public async findAll(filter: any = {}): Promise<Newsletter[]> {
    try {
      return this.newsletterRepository.find(filter);
    } catch (e) {
      throw new HttpException(
        'Newsletters find error',
        HttpStatus.INTERNAL_SERVER_ERROR,
      );
    }
  }

  public async findOne(filter: any): Promise<Newsletter> {
    try {
      return this.newsletterRepository.findOne(filter);
    } catch (e) {
      throw new HttpException(
        'Newsletter find unexpected error',
        HttpStatus.INTERNAL_SERVER_ERROR,
      );
    }
  }

  public async update(
    id: number,
    data: NewsletterParameter,
  ): Promise<Newsletter> {
    try {
      return this.newsletterRepository.save({ id, ...data });
    } catch (e) {
      throw new HttpException(
        'Newsletter update error',
        HttpStatus.INTERNAL_SERVER_ERROR,
      );
    }
  }

  public async remove(id: number): Promise<NewsletterDeleteResponse> {
    try {
      return {
        isNewsletterDeleted:
          (await this.newsletterRepository.delete({ id })).affected === 1,
      };
    } catch (e) {
      throw new HttpException(
        'Newsletter remove error',
        HttpStatus.INTERNAL_SERVER_ERROR,
      );
    }
  }
}
