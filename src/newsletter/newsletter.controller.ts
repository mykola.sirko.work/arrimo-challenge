import {
  Controller,
  Param,
  Body,
  Query,
  Get,
  Post,
  Patch,
  Delete,
} from '@nestjs/common';
import { NewsletterService } from './newsletter.service';
import { NewsletterDeleteResponse } from './responses/newsletter-delete.response';
import { NewsletterParameter } from './parameters/newsletter.parameter';
import { Newsletter } from './entities/newsletter.entity';
import { ApiTags } from '@nestjs/swagger';

@ApiTags('Newsletter')
@Controller('api/newsletter')
export class NewsletterController {
  constructor(private readonly newsletterService: NewsletterService) {}

  @Post()
  public async create(@Body() data: NewsletterParameter): Promise<Newsletter> {
    return this.newsletterService.create(data);
  }

  @Get()
  public async findAll(
    @Query() filter: NewsletterParameter,
  ): Promise<Newsletter[]> {
    return this.newsletterService.findAll({
      where: filter,
      relations: ['subscriptions'],
    });
  }

  @Get(':id')
  public async findOne(@Param('id') id: string): Promise<Newsletter> {
    return this.newsletterService.findOne({
      where: { id: +id },
      relations: ['subscriptions'],
    });
  }

  @Patch(':id')
  public async update(
    @Param('id') id: string,
    @Body() data: NewsletterParameter,
  ): Promise<Newsletter> {
    return this.newsletterService.update(+id, data);
  }

  @Delete(':id')
  public async remove(
    @Param('id') id: string,
  ): Promise<NewsletterDeleteResponse> {
    return this.newsletterService.remove(+id);
  }
}
