export interface NewsletterDeleteResponse {
  isNewsletterDeleted: boolean;
}
