import { Author } from '../../author/entities/author.entity';
import { ApiProperty } from '@nestjs/swagger';

export class NewsletterParameter {
  @ApiProperty({ type: 'string', example: 'NY News' })
  public readonly name?: string;

  @ApiProperty({ type: 'string', example: 'News for everyone' })
  public readonly description?: string;

  @ApiProperty({ type: 'number', example: 67521 })
  public readonly author: Author;
}
