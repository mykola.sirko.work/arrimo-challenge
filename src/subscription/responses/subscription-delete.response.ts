export interface SubscriptionDeleteResponse {
  isSubscriptionDeleted: boolean;
}
