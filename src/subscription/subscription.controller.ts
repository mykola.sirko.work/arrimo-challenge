import {
  Controller,
  Param,
  Body,
  Query,
  Get,
  Post,
  Patch,
  Delete,
} from '@nestjs/common';
import { SubscriptionParameter } from './parameters/subscription.parameter';
import { Subscription } from './entities/subscription.entity';
import { SubscriptionDeleteResponse } from './responses/subscription-delete.response';
import { SubscriptionService } from './subscription.service';
import { ApiResponse, ApiTags } from '@nestjs/swagger';

@ApiTags('Subscription')
@Controller('api/subscription')
export class SubscriptionController {
  constructor(private readonly subscriptionService: SubscriptionService) {}

  @Post()
  @ApiResponse({ status: 201, description: 'Created successfully' })
  @ApiResponse({ status: 500, description: 'Creation failed' })
  public async create(
    @Body() data: SubscriptionParameter,
  ): Promise<Subscription> {
    return this.subscriptionService.create(data);
  }

  @Get()
  public async findAll(
    @Query() filter: SubscriptionParameter,
  ): Promise<Subscription[]> {
    return this.subscriptionService.findAll({
      where: filter,
    });
  }

  @Get(':id')
  public async findOne(@Param('id') id: string): Promise<Subscription> {
    return this.subscriptionService.findOne({
      where: { id: +id },
    });
  }

  @Patch(':id')
  public async update(
    @Param('id') id: string,
    @Body() data: SubscriptionParameter,
  ): Promise<Subscription> {
    return this.subscriptionService.update(+id, data);
  }

  @Delete(':id')
  public async remove(
    @Param('id') id: string,
  ): Promise<SubscriptionDeleteResponse> {
    return this.subscriptionService.remove(+id);
  }
}
