import {
  Column,
  CreateDateColumn,
  Entity,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { Newsletter } from '../../newsletter/entities/newsletter.entity';

@Entity()
export class Subscription {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  email: string;

  @Column()
  isEmailVerified: boolean;

  @Column()
  fullName: string;

  @Column()
  country: string;

  @Column()
  frequency: string;

  @ManyToOne(() => Newsletter, (newsletter) => newsletter.subscriptions)
  newsletter: Newsletter;

  @UpdateDateColumn()
  updatedAt: Date;

  @CreateDateColumn()
  createdAt: Date;
}
