import { Newsletter } from '../../newsletter/entities/newsletter.entity';
import { ApiProperty } from '@nestjs/swagger';

export class SubscriptionParameter {
  @ApiProperty({ type: 'string', example: 'example@gmail.com' })
  public readonly email: string;

  @ApiProperty({ type: 'boolean', example: true })
  public readonly isEmailVerified: boolean;

  @ApiProperty({ type: 'string', example: 'J. K. Rowling' })
  public readonly fullName: string;

  @ApiProperty({ type: 'string', example: 'UA' })
  public readonly country: string;

  @ApiProperty({ type: 'string', example: 'e1w' })
  public readonly frequency: string;

  @ApiProperty({ type: 'number', example: 3368 })
  public readonly newsletter: Newsletter;
}
