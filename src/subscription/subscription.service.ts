import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { SubscriptionParameter } from './parameters/subscription.parameter';
import { Subscription } from './entities/subscription.entity';
import { SubscriptionDeleteResponse } from './responses/subscription-delete.response';

@Injectable()
export class SubscriptionService {
  constructor(
    @InjectRepository(Subscription)
    private subscriptionRepository: Repository<Subscription>,
  ) {}

  public async create(data: SubscriptionParameter): Promise<Subscription> {
    const subscription = new Subscription();

    subscription.fullName = data.fullName;
    subscription.email = data.email;
    subscription.isEmailVerified = data.isEmailVerified;
    subscription.country = data.country;
    subscription.frequency = data.frequency;
    subscription.newsletter = data.newsletter;

    try {
      return this.subscriptionRepository.save(subscription);
    } catch (e) {
      throw new HttpException(
        'Subscription create error',
        HttpStatus.INTERNAL_SERVER_ERROR,
      );
    }
  }

  public async findAll(filter: any = {}): Promise<Subscription[]> {
    try {
      return this.subscriptionRepository.find(filter);
    } catch (e) {
      throw new HttpException(
        'Subscriptions find error',
        HttpStatus.INTERNAL_SERVER_ERROR,
      );
    }
  }

  public async findOne(filter: any): Promise<Subscription> {
    try {
      return this.subscriptionRepository.findOne(filter);
    } catch (e) {
      throw new HttpException(
        'Subscription find unexpected error',
        HttpStatus.INTERNAL_SERVER_ERROR,
      );
    }
  }

  public async update(
    id: number,
    data: SubscriptionParameter,
  ): Promise<Subscription> {
    try {
      return this.subscriptionRepository.save({ id, ...data });
    } catch (e) {
      throw new HttpException(
        'Subscription update error',
        HttpStatus.INTERNAL_SERVER_ERROR,
      );
    }
  }

  public async remove(id: number): Promise<SubscriptionDeleteResponse> {
    try {
      return {
        isSubscriptionDeleted:
          (await this.subscriptionRepository.delete({ id })).affected === 1,
      };
    } catch (e) {
      throw new HttpException(
        'Subscription remove error',
        HttpStatus.INTERNAL_SERVER_ERROR,
      );
    }
  }
}
